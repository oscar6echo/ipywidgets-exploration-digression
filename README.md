## Python-Javascript two-way communication in Jupyter notebook

Based on [ipywidets](https://ipywidgets.readthedocs.io/en/latest/index.html).

### 1 - Basic

notebook: [test_js_py_com_1.ipynb](http://nbviewer.jupyter.org/urls/gitlab.com/oscar6echo/ipywidgets-exploration-digression/raw/master/test_js_py_com_1.ipynb).  
Low level but works fine.

### 2 - Arbitrary [d3.js](https://d3js.org/) based GUI

notebook: [test_js_py_com_2.ipynb](http://nbviewer.jupyter.org/urls/gitlab.com/oscar6echo/ipywidgets-exploration-digression/raw/master/test_js_py_com_2.ipynb).  
Based on this [code block](https://bl.ocks.org/mbostock/5249328).

### 3 - Question about the frontend-kernel communication channel

notebook: [test_js_py_com_3.ipynb](http://nbviewer.jupyter.org/urls/gitlab.com/oscar6echo/ipywidgets-exploration-digression/raw/master/test_js_py_com_3.ipynb).

+ What is the exact order of communication ? FIFO ?
+ Then when exactly does display inject the js in the output cell ?
+ Is there any possibility to alter the default ordering ?
+ What does the frontend-kernel protocol say ?


### 4 - Build a Mathematica-like manipulate with ipywidgets

notebook: [IPywidget_Graph.ipynb](http://nbviewer.jupyter.org/urls/gitlab.com/oscar6echo/ipywidgets-exploration-digression/raw/master/IPywidget_Graph.ipynb).  
Very convenient.